INSERT INTO T_CURRENCY
SELECT *
FROM   (SELECT 1, 'TWD', '新台幣') T
WHERE  NOT EXISTS(SELECT *
                  FROM   T_CURRENCY
                  WHERE  CODE = 'TWD'); 

INSERT INTO T_CURRENCY
SELECT *
FROM   (SELECT 2, 'USD', '美元') T
WHERE  NOT EXISTS(SELECT *
                  FROM   T_CURRENCY
                  WHERE  CODE = 'USD'); 
				  
INSERT INTO T_CURRENCY
SELECT *
FROM   (SELECT 3, 'GBP', '英鎊') T
WHERE  NOT EXISTS(SELECT *
                  FROM   T_CURRENCY
                  WHERE  CODE = 'GBP'); 

INSERT INTO T_CURRENCY
SELECT *
FROM   (SELECT 4, 'EUR', '歐元') T
WHERE  NOT EXISTS(SELECT *
                  FROM   T_CURRENCY
                  WHERE  CODE = 'EUR'); 