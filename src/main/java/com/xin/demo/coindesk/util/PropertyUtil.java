package com.xin.demo.coindesk.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.stream.Collectors;

public class PropertyUtil {

	public static Map<Object, Object> getPropertiesMap(String resourcePath) {
		Properties prop = new Properties();
		InputStream in = PropertyUtil.class.getResourceAsStream(resourcePath);
		try {
			prop.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Map<Object, Object> mapFromSet = prop.entrySet().stream()
			    .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
		return mapFromSet;
	}
}
