package com.xin.demo.coindesk.service.impl;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.xin.demo.coindesk.dto.BpiDto;
import com.xin.demo.coindesk.dto.CoindeskTransformedDataResponse;
import com.xin.demo.coindesk.dto.CurrencyInfoDto;
import com.xin.demo.coindesk.dto.CurrentPriceDto;
import com.xin.demo.coindesk.enums.CurrencyEnum;
import com.xin.demo.coindesk.service.CoindeskDataTransformService;
import com.xin.demo.coindesk.util.PropertyUtil;
import com.xin.demo.coindesk.util.RestTemplateUtil;

import net.minidev.json.JSONObject;

@Service
public class CoindeskDataTransformServiceImpl implements CoindeskDataTransformService {

	@Override
	public CurrentPriceDto callCoindeskApi() {
		Map<Object, Object> urlMap = PropertyUtil.getPropertiesMap("/url.properties");
		String reqUrl = urlMap.get("coindeskCurrentprice").toString();
		RestTemplate restTemplate = RestTemplateUtil.setConverterMediaTypeAll(new RestTemplate());
		
		// 使用Java物件mapping。
		CurrentPriceDto currentPrice = restTemplate.getForObject(reqUrl, CurrentPriceDto.class);
		return currentPrice;
	}
	
	@Override
	public CoindeskTransformedDataResponse coindeskDataTransform(CurrentPriceDto currentPrice) {
		BpiDto bpi = currentPrice.getBpi();
		// 資料轉換，此處匯率放rate_float。
		CoindeskTransformedDataResponse ctd = new CoindeskTransformedDataResponse();
		ctd.setUpdatedTime(currentPrice.getTime().getUpdated());
		CurrencyInfoDto ciUsd = new CurrencyInfoDto();
		// 可從資料庫取幣別資訊(或可如此處，使用Enum定義幣別資訊常數，見CurrencyEnum)
		ciUsd.setCurrencyCode(CurrencyEnum.USD.getCurrencyCode());
		ciUsd.setCurrencyChineseName(CurrencyEnum.USD.getCurrencyChineseName());
		ciUsd.setExchangeRate(bpi.getUsd().getRate_float());

		CurrencyInfoDto ciGbp = new CurrencyInfoDto();
		ciGbp.setCurrencyCode(CurrencyEnum.GBP.getCurrencyCode());
		ciGbp.setCurrencyChineseName(CurrencyEnum.GBP.getCurrencyChineseName());
		ciGbp.setExchangeRate(bpi.getGbp().getRate_float());

		CurrencyInfoDto ciEur = new CurrencyInfoDto();
		ciEur.setCurrencyCode(CurrencyEnum.EUR.getCurrencyCode());
		ciEur.setCurrencyChineseName(CurrencyEnum.EUR.getCurrencyChineseName());
		ciEur.setExchangeRate(bpi.getEur().getRate_float());
		
		ctd.getCurrencyInfoList().addAll(Arrays.asList(ciUsd, ciGbp, ciEur));
		
		return ctd;
	}

	@SuppressWarnings("unused")
	@Override
	public void getExchangeRateFromRter() {
		Map<Object, Object> urlMap = PropertyUtil.getPropertiesMap("/url.properties");
		String reqUrl = urlMap.get("rterCapi").toString();
		RestTemplate restTemplate = RestTemplateUtil.setConverterMediaTypeAll(new RestTemplate());
		// 此處為示範。比較好的做法是取完之後放在cache，然後固定時間間隔重新請求，而非每次請求都發查。
		JSONObject jsonObject = restTemplate.getForObject(reqUrl, JSONObject.class);
		
		// 美元 -> 台幣
		JSONObject usdToTwdInfo = ((JSONObject)jsonObject.get("USDTWD"));
		BigDecimal exrateUsdToTwd = new BigDecimal(usdToTwdInfo.get("Exrate").toString());
		String updatedTime = usdToTwdInfo.get("UTC").toString();
		
		// 美元 -> 英鎊
		JSONObject usdToGbpInfo = ((JSONObject)jsonObject.get("USDGBP"));
		BigDecimal exrateUsdToGbp = new BigDecimal(usdToGbpInfo.get("Exrate").toString());
		
		// 美元 -> 歐元
		JSONObject usdToEurInfo = ((JSONObject)jsonObject.get("USDEUR"));
		BigDecimal exrateUsdToEur = new BigDecimal(usdToEurInfo.get("Exrate").toString());
		
		// 英鎊 -> 台幣
		BigDecimal exrateGbpToTwd = exrateUsdToTwd.divide(exrateUsdToGbp);
		
		// 歐元 -> 台幣
		BigDecimal exrateEurToTwd = exrateUsdToTwd.divide(exrateUsdToEur);
	}

}
