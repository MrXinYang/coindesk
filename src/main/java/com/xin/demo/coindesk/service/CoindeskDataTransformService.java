package com.xin.demo.coindesk.service;

import com.xin.demo.coindesk.dto.CoindeskTransformedDataResponse;
import com.xin.demo.coindesk.dto.CurrentPriceDto;

public interface CoindeskDataTransformService {

	/**
	 * 呼叫coindesk的API。
	 */
	public CurrentPriceDto callCoindeskApi();
	
	/**
	 * 對CurrentPrice進行資料轉換為CoindeskTransformedDataResponse物件。
	 */
	public CoindeskTransformedDataResponse coindeskDataTransform(CurrentPriceDto currentPrice);
	
	/**
	 * TODO，若有需要取得最新匯率可使用，return type可調整。
	 */
	public void getExchangeRateFromRter();

}
