package com.xin.demo.coindesk.service.impl;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xin.demo.coindesk.dto.AddCurrencyRequest;
import com.xin.demo.coindesk.dto.AddCurrencyResponse;
import com.xin.demo.coindesk.dto.DelCurrencyRequest;
import com.xin.demo.coindesk.dto.DelCurrencyResponse;
import com.xin.demo.coindesk.dto.QueryCurrencyRequest;
import com.xin.demo.coindesk.dto.QueryCurrencyResponse;
import com.xin.demo.coindesk.dto.UpdateCurrencyRequest;
import com.xin.demo.coindesk.dto.UpdateCurrencyResponse;
import com.xin.demo.coindesk.entity.Currency;
import com.xin.demo.coindesk.repository.CurrencyRepository;
import com.xin.demo.coindesk.service.CurrencyService;

@Service
public class CurrencyServiceImpl implements CurrencyService {

	@Autowired
	private CurrencyRepository CurrencyRepository;

	@Override
	public AddCurrencyResponse add(AddCurrencyRequest request) {
		Currency currency = new Currency();
		currency.setCode(request.getCode());
		currency.setNameTw(request.getNameTw());
		System.out.println(currency);
		CurrencyRepository.save(currency);
		AddCurrencyResponse response = new AddCurrencyResponse();
		response.setStatus("Add success");
		return response;
	}

	@Override
	public DelCurrencyResponse del(DelCurrencyRequest request) {
		Optional<Currency> currencyOptional = CurrencyRepository.findById(Long.valueOf(request.getId()));
		DelCurrencyResponse response = new DelCurrencyResponse();
		if (currencyOptional.isPresent()) {
			Currency currency = currencyOptional.get();
			CurrencyRepository.deleteById(currency.getId());
			response.setStatus("del Success");
		} else {
			response.setStatus("del failed");
		}
		return response;
	}

	@Override
	public UpdateCurrencyResponse update(UpdateCurrencyRequest request) {
		Optional<Currency> currencyOptional = CurrencyRepository.findById(Long.valueOf(request.getId()));
		UpdateCurrencyResponse response = new UpdateCurrencyResponse();
		if (currencyOptional.isPresent()) {
			Currency currency = currencyOptional.get();
			currency.setCode(request.getCode());
			currency.setNameTw(request.getNameTw());
			CurrencyRepository.save(currency);
			response.setStatus("Update Success");
		} else {
			response.setStatus("Update failed");
		}
		return response;
	}

	@Override
	public QueryCurrencyResponse queryAll() {
		QueryCurrencyResponse response = new QueryCurrencyResponse();
		ArrayList<Currency> CurrencyList = (ArrayList<Currency>) CurrencyRepository.findAll();
		response.setCurrencyList(CurrencyList);
		return response;
	}
	
	@Override
	public QueryCurrencyResponse queryById(QueryCurrencyRequest request) {
		Optional<Currency> currencyOptional = CurrencyRepository.findById(Long.valueOf(request.getId()));
		QueryCurrencyResponse response = new QueryCurrencyResponse();
		if (currencyOptional.isPresent()) {
			ArrayList<Currency> currencyList = new ArrayList<Currency>();
			currencyList.add(currencyOptional.get());
			response.setCurrencyList(currencyList);
		}
		return response;
	}
}
