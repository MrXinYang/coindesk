package com.xin.demo.coindesk.service;

import com.xin.demo.coindesk.dto.*;

public interface CurrencyService {

	public AddCurrencyResponse add(AddCurrencyRequest request);

	public DelCurrencyResponse del(DelCurrencyRequest request);

	public UpdateCurrencyResponse update(UpdateCurrencyRequest request);

	public QueryCurrencyResponse queryAll();
	
	public QueryCurrencyResponse queryById(QueryCurrencyRequest request);
}
