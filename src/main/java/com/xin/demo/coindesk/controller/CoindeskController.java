package com.xin.demo.coindesk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xin.demo.coindesk.dto.CoindeskTransformedDataResponse;
import com.xin.demo.coindesk.dto.CurrentPriceDto;
import com.xin.demo.coindesk.service.impl.CoindeskDataTransformServiceImpl;

@RestController
@RequestMapping(path = "/api/coindesk", produces = MediaType.APPLICATION_JSON_VALUE)
public class CoindeskController {

	@Autowired
	private CoindeskDataTransformServiceImpl coindeskDataTransformService;

	@PostMapping(value = "/callApi")
	public CurrentPriceDto getCurrentPriceDto() {

		return coindeskDataTransformService.callCoindeskApi();
	}

	@PostMapping(value = "/callApiAndTransform")
	public CoindeskTransformedDataResponse getTransformedDataResponse() {

		return coindeskDataTransformService.coindeskDataTransform(coindeskDataTransformService.callCoindeskApi());
	}

}
