package com.xin.demo.coindesk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xin.demo.coindesk.dto.*;
import com.xin.demo.coindesk.service.impl.CurrencyServiceImpl;

@RestController
@RequestMapping(path = "/api/currency", produces = MediaType.APPLICATION_JSON_VALUE)
public class CurrencyController {

	@Autowired
	private CurrencyServiceImpl currencyService;

	@PostMapping(value = "/add")
	public AddCurrencyResponse getAddResponse(@RequestBody AddCurrencyRequest request) {

		return currencyService.add(request);
	}

	@PostMapping(value = "/del")
	public DelCurrencyResponse getDelResponse(@RequestBody DelCurrencyRequest request) {

		return currencyService.del(request);
	}

	@PostMapping(value = "/update")
	public UpdateCurrencyResponse getUpdateResponse(@RequestBody UpdateCurrencyRequest request) {

		return currencyService.update(request);
	}

	@PostMapping(value = "/queryAll")
	public QueryCurrencyResponse getQueryAllResponse() {

		return currencyService.queryAll();
	}

	@PostMapping(value = "/queryById")
	public QueryCurrencyResponse getQueryByIdResponse(@RequestBody QueryCurrencyRequest request) {

		return currencyService.queryById(request);
	}
}
