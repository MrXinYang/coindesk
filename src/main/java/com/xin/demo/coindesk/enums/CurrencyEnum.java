package com.xin.demo.coindesk.enums;

public enum CurrencyEnum {
	TWD("TWD", "新台幣"), USD("USD", "美元"), GBP("GBP", "英鎊"), EUR("EUR", "歐元");

	private String currencyChineseName;
	private String currencyCode;

	CurrencyEnum(String cc, String ccn) {
		this.currencyCode = cc;
		this.currencyChineseName = ccn;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public String getCurrencyChineseName() {
		return currencyChineseName;
	}
}
