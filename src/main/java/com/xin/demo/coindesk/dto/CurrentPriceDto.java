package com.xin.demo.coindesk.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class CurrentPriceDto {
	
	@JsonProperty("time")
	private TimeDto time;
	private String disclaimer;
	private String chartName;
	@JsonProperty("bpi")
	private BpiDto bpi;

}
