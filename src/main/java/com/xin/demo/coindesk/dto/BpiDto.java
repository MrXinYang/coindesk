package com.xin.demo.coindesk.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BpiDto {
	
	@JsonProperty("USD")
	private CurrencyDto usd;
	@JsonProperty("GBP")
	private CurrencyDto gbp;
	@JsonProperty("EUR")
	private CurrencyDto eur;
	
}
