package com.xin.demo.coindesk.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.xin.demo.coindesk.entity.Currency;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class QueryCurrencyResponse {

    private List<Currency> currencyList;
}
