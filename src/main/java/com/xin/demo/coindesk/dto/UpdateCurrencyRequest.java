package com.xin.demo.coindesk.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class UpdateCurrencyRequest {

    @JsonProperty("ID")
    private String id;
    @JsonProperty("CODE")
    private String code;
    @JsonProperty("NAME_TW")
    private String nameTw;
    
}
