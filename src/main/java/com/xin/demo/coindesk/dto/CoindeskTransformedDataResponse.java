package com.xin.demo.coindesk.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
public class CoindeskTransformedDataResponse {
	
	private String updatedTime;
	private List<CurrencyInfoDto> currencyInfoList;
	
	public CoindeskTransformedDataResponse(){
		super();
		this.currencyInfoList = new ArrayList<CurrencyInfoDto>();
	}
}
