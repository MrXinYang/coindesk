package com.xin.demo.coindesk.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddCurrencyRequest {

    @JsonProperty("CODE")
    private String code;
    @JsonProperty("NAME_TW")
    private String nameTw;
}
