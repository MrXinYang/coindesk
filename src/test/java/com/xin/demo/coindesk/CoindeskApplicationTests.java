package com.xin.demo.coindesk;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xin.demo.coindesk.dto.AddCurrencyRequest;
import com.xin.demo.coindesk.dto.DelCurrencyRequest;
import com.xin.demo.coindesk.dto.QueryCurrencyRequest;
import com.xin.demo.coindesk.dto.UpdateCurrencyRequest;

//@WebMvcTest
//@SpringBootTest
//@TestConfiguration
//@AutoConfigureMockMvc
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
class CoindeskApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void callCoindeskApiTest() throws Exception {
		ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/api/coindesk/callApi"))
				.andExpect(MockMvcResultMatchers.status().isOk());
		resultActions.andReturn().getResponse().setCharacterEncoding("UTF-8");
		resultActions.andDo(MockMvcResultHandlers.print());
	}

	@Test
	public void callCoindeskApiAndTransformTest() throws Exception {
		ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/api/coindesk/callApiAndTransform"))
				.andExpect(MockMvcResultMatchers.status().isOk());
		resultActions.andReturn().getResponse().setCharacterEncoding("UTF-8");
		resultActions.andDo(MockMvcResultHandlers.print());
	}

	@Test
	public void addCurrencyTest() throws Exception {
		AddCurrencyRequest request = new AddCurrencyRequest();
		request.setCode("OMG");
		request.setNameTw("歐買尬");
		System.out.println(objectMapper.writeValueAsString(request));
		// 用json物件打post，localhost:8080/api/currency/add。
//		ResultActions resultActions = mockMvc
//				.perform(MockMvcRequestBuilders.post("/api/currency/add").contentType("application/json")
//						.content(objectMapper.writeValueAsString(request)))
//				.andExpect(MockMvcResultMatchers.status().isOk());
//		resultActions.andReturn().getResponse().setCharacterEncoding("UTF-8");
//		resultActions.andDo(MockMvcResultHandlers.print());
	}

	@Test
	public void delCurrencyTest() throws JsonProcessingException {
		DelCurrencyRequest request = new DelCurrencyRequest();
		request.setId("5");
		System.out.println(objectMapper.writeValueAsString(request));
		// 打post，給參數，略。
	}

	@Test
	public void updateCurrencyTest() throws JsonProcessingException {
		UpdateCurrencyRequest request = new UpdateCurrencyRequest();
		request.setId("5");
		request.setCode("QOO");
		request.setNameTw("有種果汁真好喝");
		System.out.println(objectMapper.writeValueAsString(request));
		// 打post，給參數，略。
	}

	@Test
	public void queryCurrencyTest() {
		// 打post，不用給參數，略。
	}

	@Test
	public void queryAllCurrencyTest() throws JsonProcessingException {
		QueryCurrencyRequest request = new QueryCurrencyRequest();
		request.setId("5");
		System.out.println(objectMapper.writeValueAsString(request));
		// 打post，給參數，略。
	}

}
